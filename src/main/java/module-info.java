module com.example.ong_app {
    requires javafx.controls;
    requires javafx.fxml;
    requires javax.persistence;
    requires java.sql;


    opens com.example.ong_app to javafx.fxml;
    exports com.example.ong_app;
}