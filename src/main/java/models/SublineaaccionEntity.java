package models;

import javax.persistence.*;

@Entity
@Table(name = "sublineaaccion", schema = "db_spm", catalog = "")
public class SublineaaccionEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idSublinea", nullable = false)
    private Integer idSublinea;
    @Basic
    @Column(name = "concepto", nullable = false, length = 100)
    private String concepto;
    @Basic
    @Column(name = "idLinea", nullable = false)
    private Integer idLinea;

    public Integer getIdSublinea() {
        return idSublinea;
    }

    public void setIdSublinea(Integer idSublinea) {
        this.idSublinea = idSublinea;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Integer getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(Integer idLinea) {
        this.idLinea = idLinea;
    }
}
