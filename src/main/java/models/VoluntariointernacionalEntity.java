package models;

import javax.persistence.*;

@Entity
@Table(name = "voluntariointernacional", schema = "db_spm", catalog = "")
public class VoluntariointernacionalEntity {
    @Basic
    @Column(name = "pais", nullable = false, length = 50)
    private String pais;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idEquipo", nullable = false)
    private Integer idEquipo;

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Integer getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Integer idEquipo) {
        this.idEquipo = idEquipo;
    }
}
