package models;

import javax.persistence.*;

@Entity
@Table(name = "ingreso_publico", schema = "db_spm", catalog = "")
public class IngresoPublicoEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idIngreso", nullable = false)
    private Integer idIngreso;
    @Basic
    @Column(name = "tipoAportador", nullable = false, length = 50)
    private String tipoAportador;

    public Integer getIdIngreso() {
        return idIngreso;
    }

    public void setIdIngreso(Integer idIngreso) {
        this.idIngreso = idIngreso;
    }

    public String getTipoAportador() {
        return tipoAportador;
    }

    public void setTipoAportador(String tipoAportador) {
        this.tipoAportador = tipoAportador;
    }
}
