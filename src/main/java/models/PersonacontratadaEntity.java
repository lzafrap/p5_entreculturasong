package models;

import javax.persistence.*;

@Entity
@Table(name = "personacontratada", schema = "db_spm", catalog = "")
public class PersonacontratadaEntity {
    @Basic
    @Column(name = "sueldo", nullable = false)
    private Integer sueldo;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idEquipo", nullable = false)
    private Integer idEquipo;

    public Integer getSueldo() {
        return sueldo;
    }

    public void setSueldo(Integer sueldo) {
        this.sueldo = sueldo;
    }

    public Integer getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Integer idEquipo) {
        this.idEquipo = idEquipo;
    }
}
