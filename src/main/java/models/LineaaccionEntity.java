package models;

import javax.persistence.*;

@Entity
@Table(name = "lineaaccion", schema = "db_spm", catalog = "")
public class LineaaccionEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idLinea", nullable = false)
    private Integer idLinea;
    @Basic
    @Column(name = "tipoLinea", nullable = false)
    private Object tipoLinea;

    public Integer getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(Integer idLinea) {
        this.idLinea = idLinea;
    }

    public Object getTipoLinea() {
        return tipoLinea;
    }

    public void setTipoLinea(Object tipoLinea) {
        this.tipoLinea = tipoLinea;
    }
}
