package models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "proyecto", schema = "db_spm", catalog = "")
public class ProyectoEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "IdProyecto", nullable = false)
    private Integer idProyecto;
    @Basic
    @Column(name = "Pais", nullable = false, length = 64)
    private String pais;
    @Basic
    @Column(name = "Localizacion", nullable = false, length = 100)
    private String localizacion;
    @Basic
    @Column(name = "FechaInicio", nullable = false)
    private Date fechaInicio;
    @Basic
    @Column(name = "FechaFinal", nullable = true)
    private Date fechaFinal;
    @Basic
    @Column(name = "Financiador", nullable = false, length = 100)
    private String financiador;
    @Basic
    @Column(name = "FinanciacionAportada", nullable = false, precision = 0)
    private Double financiacionAportada;
    @Basic
    @Column(name = "SocioLocal", nullable = false, length = 100)
    private String socioLocal;
    @Basic
    @Column(name = "idLinea", nullable = false)
    private Integer idLinea;
    @Basic
    @Column(name = "idONG", nullable = false)
    private Integer idOng;

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public String getFinanciador() {
        return financiador;
    }

    public void setFinanciador(String financiador) {
        this.financiador = financiador;
    }

    public Double getFinanciacionAportada() {
        return financiacionAportada;
    }

    public void setFinanciacionAportada(Double financiacionAportada) {
        this.financiacionAportada = financiacionAportada;
    }

    public String getSocioLocal() {
        return socioLocal;
    }

    public void setSocioLocal(String socioLocal) {
        this.socioLocal = socioLocal;
    }

    public Integer getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(Integer idLinea) {
        this.idLinea = idLinea;
    }

    public Integer getIdOng() {
        return idOng;
    }

    public void setIdOng(Integer idOng) {
        this.idOng = idOng;
    }
}
