package models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "equipo", schema = "db_spm")
public class EquipoEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idEquipo", nullable = false)
    private Integer idEquipo;
    @Basic
    @Column(name = "nombre", nullable = false, length = 100)
    private String nombre;
    @Basic
    @Column(name = "apellido1", nullable = false, length = 100)
    private String apellido1;
    @Basic
    @Column(name = "apellido2", nullable = true, length = 100)
    private String apellido2;
    @Basic
    @Column(name = "altaMiembroEquipo", nullable = false)
    private Date altaMiembroEquipo;
    @Basic
    @Column(name = "bajaMiembroEquipo", nullable = true)
    private Date bajaMiembroEquipo;
    @Basic
    @Column(name = "telefono", nullable = true, length = 15)
    private String telefono;
    @Basic
    @Column(name = "usuario", nullable = false, length = 45)
    private String usuario;
    @Basic
    @Column(name = "password", nullable = false, length = 45)
    private String password;
    @Basic
    @Column(name = "rol", nullable = false)
    private Object rol;
    @Basic
    @Column(name = "idONG", nullable = false)
    private Integer idOng;
    @Basic
    @Column(name = "DTYPE", nullable = false, length = 31)
    private String dtype;
    @Basic
    @Column(name = "horasPorSemana", nullable = true, precision = 0)
    private Double horasPorSemana;

    public Integer getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Integer idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public Date getAltaMiembroEquipo() {
        return altaMiembroEquipo;
    }

    public void setAltaMiembroEquipo(Date altaMiembroEquipo) {
        this.altaMiembroEquipo = altaMiembroEquipo;
    }

    public Date getBajaMiembroEquipo() {
        return bajaMiembroEquipo;
    }

    public void setBajaMiembroEquipo(Date bajaMiembroEquipo) {
        this.bajaMiembroEquipo = bajaMiembroEquipo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Object getRol() {
        return rol;
    }

    public void setRol(Object rol) {
        this.rol = rol;
    }

    public Integer getIdOng() {
        return idOng;
    }

    public void setIdOng(Integer idOng) {
        this.idOng = idOng;
    }

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    public Double getHorasPorSemana() {
        return horasPorSemana;
    }

    public void setHorasPorSemana(Double horasPorSemana) {
        this.horasPorSemana = horasPorSemana;
    }
}
