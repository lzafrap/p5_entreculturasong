package models;

import javax.persistence.*;

@Entity
@Table(name = "proyecto_equipo", schema = "db_spm", catalog = "")
@IdClass(ProyectoEquipoEntityPK.class)
public class ProyectoEquipoEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idEquipo", nullable = false)
    private Integer idEquipo;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "IdProyecto", nullable = false)
    private Integer idProyecto;

    public Integer getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Integer idEquipo) {
        this.idEquipo = idEquipo;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }
}
