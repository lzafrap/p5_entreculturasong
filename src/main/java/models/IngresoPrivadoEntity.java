package models;

import javax.persistence.*;

@Entity
@Table(name = "ingreso_privado", schema = "db_spm", catalog = "")
public class IngresoPrivadoEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idIngreso", nullable = false)
    private Integer idIngreso;
    @Basic
    @Column(name = "origenAportacion", nullable = false, length = 50)
    private String origenAportacion;
    @Basic
    @Column(name = "idSocio", nullable = false)
    private Integer idSocio;

    public Integer getIdIngreso() {
        return idIngreso;
    }

    public void setIdIngreso(Integer idIngreso) {
        this.idIngreso = idIngreso;
    }

    public String getOrigenAportacion() {
        return origenAportacion;
    }

    public void setOrigenAportacion(String origenAportacion) {
        this.origenAportacion = origenAportacion;
    }

    public Integer getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(Integer idSocio) {
        this.idSocio = idSocio;
    }
}
