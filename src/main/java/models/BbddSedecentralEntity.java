package models;

import javax.persistence.*;

@Entity
@Table(name = "bbdd_sedecentral", schema = "db_spm", catalog = "")
public class BbddSedecentralEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idONG", nullable = false)
    private Integer idOng;

    public Integer getIdOng() {
        return idOng;
    }

    public void setIdOng(Integer idOng) {
        this.idOng = idOng;
    }
}
