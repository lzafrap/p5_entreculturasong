package models;

import javax.persistence.*;

@Entity
@Table(name = "bbdd_ong", schema = "db_spm", catalog = "")
public class BbddOngEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idONG", nullable = false)
    private Integer idOng;
    @Basic
    @Column(name = "nombreSede", nullable = false, length = 50)
    private String nombreSede;
    @Basic
    @Column(name = "ubicacion", nullable = false, length = 50)
    private String ubicacion;

    public Integer getIdOng() {
        return idOng;
    }

    public void setIdOng(Integer idOng) {
        this.idOng = idOng;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
