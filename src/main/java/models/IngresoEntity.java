package models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "ingreso", schema = "db_spm", catalog = "")
public class IngresoEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idIngreso", nullable = false)
    private Integer idIngreso;
    @Basic
    @Column(name = "cantidad", nullable = false, precision = 0)
    private Double cantidad;
    @Basic
    @Column(name = "tipoAportacion", nullable = false, length = 50)
    private String tipoAportacion;
    @Basic
    @Column(name = "fechaIngreso", nullable = true)
    private Date fechaIngreso;
    @Basic
    @Column(name = "nombreAportador", nullable = false, length = 50)
    private String nombreAportador;
    @Basic
    @Column(name = "idAportador", nullable = true, length = 50)
    private String idAportador;
    @Basic
    @Column(name = "idONG", nullable = false)
    private Integer idOng;

    public Integer getIdIngreso() {
        return idIngreso;
    }

    public void setIdIngreso(Integer idIngreso) {
        this.idIngreso = idIngreso;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public String getTipoAportacion() {
        return tipoAportacion;
    }

    public void setTipoAportacion(String tipoAportacion) {
        this.tipoAportacion = tipoAportacion;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getNombreAportador() {
        return nombreAportador;
    }

    public void setNombreAportador(String nombreAportador) {
        this.nombreAportador = nombreAportador;
    }

    public String getIdAportador() {
        return idAportador;
    }

    public void setIdAportador(String idAportador) {
        this.idAportador = idAportador;
    }

    public Integer getIdOng() {
        return idOng;
    }

    public void setIdOng(Integer idOng) {
        this.idOng = idOng;
    }
}
