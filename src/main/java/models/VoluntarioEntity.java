package models;

import javax.persistence.*;

@Entity
@Table(name = "voluntario", schema = "db_spm", catalog = "")
public class VoluntarioEntity {
    @Basic
    @Column(name = "horasPorSemana", nullable = false, precision = 0)
    private Double horasPorSemana;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idEquipo", nullable = false)
    private Integer idEquipo;

    public Double getHorasPorSemana() {
        return horasPorSemana;
    }

    public void setHorasPorSemana(Double horasPorSemana) {
        this.horasPorSemana = horasPorSemana;
    }

    public Integer getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Integer idEquipo) {
        this.idEquipo = idEquipo;
    }
}
