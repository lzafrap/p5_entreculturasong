package models;

import javax.persistence.*;

@Entity
@Table(name = "colaborador", schema = "db_spm", catalog = "")
public class ColaboradorEntity {
    @Basic
    @Column(name = "tipoColaboracion", nullable = false, length = 100)
    private String tipoColaboracion;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "idEquipo", nullable = false)
    private Integer idEquipo;

    public String getTipoColaboracion() {
        return tipoColaboracion;
    }

    public void setTipoColaboracion(String tipoColaboracion) {
        this.tipoColaboracion = tipoColaboracion;
    }

    public Integer getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Integer idEquipo) {
        this.idEquipo = idEquipo;
    }
}
