package models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "socio", schema = "db_spm", catalog = "")
public class SocioEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "IdSocio", nullable = false)
    private Integer idSocio;
    @Basic
    @Column(name = "Nombre", nullable = false, length = 45)
    private String nombre;
    @Basic
    @Column(name = "Apellido1", nullable = false, length = 45)
    private String apellido1;
    @Basic
    @Column(name = "Apellido2", nullable = true, length = 45)
    private String apellido2;
    @Basic
    @Column(name = "NIF", nullable = false, length = 11)
    private String nif;
    @Basic
    @Column(name = "Dirección", nullable = true, length = 200)
    private String dirección;
    @Basic
    @Column(name = "FechaNacimiento", nullable = true)
    private Date fechaNacimiento;
    @Basic
    @Column(name = "Movil", nullable = true, length = 12)
    private String movil;
    @Basic
    @Column(name = "Correo", nullable = true, length = 100)
    private String correo;
    @Basic
    @Column(name = "Sexo", nullable = true)
    private Object sexo;
    @Basic
    @Column(name = "tipoCuota", nullable = false)
    private Object tipoCuota;
    @Basic
    @Column(name = "FechaAlta", nullable = false)
    private Date fechaAlta;
    @Basic
    @Column(name = "FechaBaja", nullable = true)
    private Date fechaBaja;
    @Basic
    @Column(name = "NroCuenta", nullable = false, length = 50)
    private String nroCuenta;
    @Basic
    @Column(name = "idONG", nullable = false)
    private Integer idOng;
    @Basic
    @Column(name = "cuentaBancaria", nullable = true, length = 255)
    private String cuentaBancaria;

    public Integer getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(Integer idSocio) {
        this.idSocio = idSocio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getDirección() {
        return dirección;
    }

    public void setDirección(String dirección) {
        this.dirección = dirección;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Object getSexo() {
        return sexo;
    }

    public void setSexo(Object sexo) {
        this.sexo = sexo;
    }

    public Object getTipoCuota() {
        return tipoCuota;
    }

    public void setTipoCuota(Object tipoCuota) {
        this.tipoCuota = tipoCuota;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public Integer getIdOng() {
        return idOng;
    }

    public void setIdOng(Integer idOng) {
        this.idOng = idOng;
    }

    public String getCuentaBancaria() {
        return cuentaBancaria;
    }

    public void setCuentaBancaria(String cuentaBancaria) {
        this.cuentaBancaria = cuentaBancaria;
    }
}
